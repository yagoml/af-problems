export interface Problem {
  id: number
  name: string
  description: string
}

export interface Measure {
  height: string
  growth: string
}

export interface UsrType {
  id: number
  name: string
  term: number
}

export interface UsrTypes {
  [key: string]: UsrType
  teacher: UsrType
}

export interface Coordinate {
  x: number
  y: number
}

export interface NumberCache {
  [key: number]: number
}
