import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Problems from '../views/Problems.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/problem/:id',
    name: 'Problems',
    component: Problems
  }
]

const router = new VueRouter({
  routes
})

export default router
