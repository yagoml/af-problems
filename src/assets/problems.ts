import { Problem } from '@/interfaces'

export default {
  growth: {
    id: 1,
    name: 'Crescimento',
    description: `Chico tem 1,50m e cresce 2 centímetros por ano, enquanto Juca tem
          1,10m e cresce 3 centímetros por ano. Construir um algoritmo que
          calcule e imprima quantos anos serão necessários para que Juca seja
          maior que Chico.`
  } as Problem,
  library: {
    id: 2,
    name: 'Biblioteca',
    description: `A biblioteca de uma universidade deseja fazer um algoritmo que leia o
          nome do livro que será emprestado, o tipo de usuário (professor ou
          aluno), o algoritmo deve imprimir um recibo mostrando o nome do livro,
          o tipo de usuário por extenso e o total de dias de empréstimo.
          <br />Considerar que o professor tem 10 dias para devolver o livro o
          aluno somente 3 dias.`
  } as Problem,
  matriz: {
    id: 3,
    name: 'Matriz',
    description: `Criar um algoritmo com uma matriz 5x5 e imprima: toda a matriz, a
          matriz gerada só com números ímpares e outra só com números pares.`
  } as Problem,
  fibonacci: {
    id: 4,
    name: 'Fibonacci',
    description: `Criar um algoritmo com um campo que possa receber apenas números e
          vírgulas, separe os valores enviados e valide aqueles que são um
          número válido da Sequência de Fibonacci e imprima os números de forma
          crescente, conforme o exemplo:
          <br />CAMPO RECEBE: 1,13,55,21,5,83 <br />IMPRIME: 1,5,13,21,55`
  } as Problem
}
